% prolog
% 4x4 sudoku solver
val(1).
val(2).
val(3).
val(4).
board(A,B,C,D,
    E,F,G,H,
    I,J,K,L,
    M,N,O,P) :-
    square(A,B,E,F),
    square(C,D,G,H),
    square(A,B,C,D),
    square(I,J,M,N),
    square(K,L,O,P),
    square(E,F,G,H),
    square(I,J,K,L),
    square(M,N,O,P),
    square(A,E,I,M),
    square(B,F,J,N),
    square(C,G,K,O),
    square(D,H,L,P).
square(A,B,C,D) :-
    val(A),val(B), A \= B, val(C),B \= C,A \= C,val(D),
    ,  C \= D,
     A \= D, B \= D.
